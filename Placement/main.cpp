#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;

class Point{
    public:
        void set_coord(int x, int y){
            x_coord = x;
            y_coord = y;
        }
        int get_coord(char choice){
            if (choice == 'x')
                return x_coord;
            if (choice == 'y')
                return y_coord;
            else
                return 0;
        }
        void set_name(string nameofpoint){
            name = nameofpoint;
        }
        string get_name(){
            return name;
        }
    private:
        int x_coord, y_coord;
        string name;
};

class Cell{
    // Cell is 6 gridspace wide, 6 gridspace long square with two terminals on the top edge and two terminals on the bottom edge.
    public:
        void set_terminals(Point terminal, int location){
            // by applying the constraints to only one known terminal point, you can set or reset the cartesian coordinates of all other terminals.
            if (location == 1){
                terminal_1.set_coord(terminal.get_coord('x') , terminal.get_coord('y'));
                terminal_2.set_coord(terminal_1.get_coord('x')+3 , terminal_1.get_coord('y'));
                terminal_3.set_coord(terminal_1.get_coord('x'), terminal_1.get_coord('y')-5);
                terminal_4.set_coord(terminal_1.get_coord('x')+3, terminal_1.get_coord('y')-5);
            }
            if (location == 2){
                terminal_2.set_coord(terminal.get_coord('x'), terminal.get_coord('y'));
                terminal_1.set_coord(terminal_2.get_coord('x')-3, terminal_2.get_coord('y'));
                terminal_3.set_coord(terminal_2.get_coord('x')-3, terminal_2.get_coord('y')-5);
                terminal_4.set_coord(terminal_2.get_coord('x'), terminal_2.get_coord('y')-5);
            }
            if (location == 3){
                terminal_3.set_coord(terminal.get_coord('x'), terminal.get_coord('y'));
                terminal_1.set_coord(terminal_3.get_coord('x'), terminal_3.get_coord('y')+5);
                terminal_2.set_coord(terminal_3.get_coord('x')+3, terminal_3.get_coord('y')+5);
                terminal_4.set_coord(terminal_3.get_coord('x')+3, terminal_3.get_coord('y'));
            }
            if (location == 4){
                terminal_4.set_coord(terminal.get_coord('x'),terminal.get_coord('y'));
                terminal_1.set_coord(terminal_4.get_coord('x')-3, terminal_4.get_coord('y')+5);
                terminal_2.set_coord(terminal_4.get_coord('x'), terminal_4.get_coord('y')+5);
                terminal_3.set_coord(terminal_4.get_coord('x')-3, terminal_4.get_coord('y'));
            }
        }
        void set_name(int number){
            cellNum = number;
        }
        int get_name(){
            return cellNum;
        }
    private:
        Point terminal_1, terminal_2, terminal_3, terminal_4;
        int cellNum;
};

class Net{
    public:

        void set_name(int number){
            netNum = number;
        }
        int get_name(){
            return netNum;
        }

    private:
        //probably use pointers here which point to cells...
        int netNum, distance; // integer netNumber given by user. length of net.
};



int main() {
      int i=0, j=0, N=0, C=0;
      cout << "input number of nets: " << endl;
      cin >> N;
      cout << "input number of cells: " << endl;
      cin >> C;
      int arr[5][N]; // array to hold inputs of the user's net configurations.

      cout << "- - - - - - - - - - - -" << endl;
      cout << "Net Specification" << endl;
      cout << "NetNum CellNum TerminalNum CellNum TerminalNum" << endl;
      string temp;
      for(i = 0; i < N; i++) {
        scanf("%d %d %d %d %d", &arr[0][i], &arr[1][i], &arr[2][i], &arr[3][i], &arr[4][i]); // user configuration input...
        if ((arr[0][i] != i+1) || (arr[1][i] > C) || (arr[2][i] > 4) || (arr[3][i] > C) || (arr[4][i] > 4)) {
            cout << "Invalid input, please redo last net configuration..." << endl; // input error catch and handling...
            i = i - 1;
        }
        else {
            temp = "";
        }
      }
}
